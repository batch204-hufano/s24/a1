// alert("Hello");

const num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

/************************************************/

const address = [258, "Washington Ave NW", "California", 90011];

const [addressNumber, street , state , zipCode] = address

console.log(`I live at ${addressNumber} ${street}, ${state} ${zipCode}`);

/************************************************/

const animal = {
	name: "Lolong",
	habitat: "saltwater crocodile",
	weight: "1075 kgs",
	measured: "20 ft 3in"

}

const {name, habitat, weight, measured} = animal

console.log(`${name} was a ${habitat}. He weighed at ${weight} with a measurement of ${measured}.`);

/************************************************/

const numbers = [1, 2, 3, 4, 5]

numbers.forEach((number) => {
	console.log(number);
});

/************************************************/

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
